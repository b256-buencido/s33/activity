console.log("Hello");

// Create a fetch request using the GET method that will retrieve ALL to do list items from JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})
.then(response => response.json())

.then(json => {
	// Using the data retrieved, create an array using the "map" method to return ONLY the title of every item and print the result in the console
	let titleArray  = json.map(item => item.title);
	console.log(titleArray);
	// console.log(typeof titleArray)
});

// Create a fetch request using GET method that will retrieve a single to do list item from JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "GET"})
.then(response => response.json())
.then(json => {

	// Using the data retrieved, print a message that will provide the title and status of the to do list item
	let title = json.title;

	let status;
	if(json.completed) {
		status = "true"
	}
	else{
		status = "false"
	}

	console.log(`The item "${title}" on the list has a status of ${status}`);
	// console.log(json)
})

// Create a fetch request using POST method that will create a to do list item using the JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos/", {
	method: "POST",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		id: 201,
		userId: 1,
		completed: false
	})
})
.then(response => (response.json()))
.then(json => (console.log(json)));

// Create a fetch request using PUT method that will update a to do list item using the JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method:"PUT",
	headers:{
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		userId: 1,
		status: "Pending",
		dateCompleted:"Pending"
	})
})
.then(response => (response.json()))
.then(json => (console.log(json)));

// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method:"PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	// Update a to do list item by changing the status to complete and add a date when the status was changed.
	body: JSON.stringify({
		title: "delectus aut autem",
		userId: 1,
		status: "Complete",
		dateCompleted: "03/28/23"
	})
})
.then(response => (response.json()))
.then(json => (console.log(json)));

// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "DELETE"
});

